#!/usr/bin/env python3

from os.path import exists
from shutil import which
from subprocess import run
import pandas as pd
import math

def LoadRuninfo(path):
    converters = {"ID":str}
    # Infer input format from extension
    if path.endswith(".csv"):
        runinfo = pd.read_csv(path, converters = converters)
    elif path.endswith(".tsv"):
        runinfo = pd.read_table(path, converters = converters)
    elif path.endswith(".xlsx"):
        runinfo = pd.read_excel(path, converters = converters)
    else:
        raise Exception(f"Extension not recognized: {path}")

    if not {"ID", "Paired", "Reads"}.issubset(runinfo.columns):
        raise Exception(f"Missing the 'ID', 'Paired', and/or 'Reads' columns in {path}")

    runinfo["Paired"] = runinfo["Paired"].str.lower()
    if not all(x in {"yes", "no"} for x in runinfo["Paired"]):
        raise Exception(f"The 'Paired' column contains values other than 'yes'/'no' in {path}")

    runinfo = runinfo.set_index("ID")
    return runinfo

def GetReads(sample, runinfo, delim, folder):
    reads = runinfo["Reads"].loc[sample].split(delim)
    paths = [folder + read for read in reads]
    return(paths)

def LoadGenomeIdx(genome):
    idx = genome + ".fai"
    faiHeader = ["contig", "size", "byte_start", "bp_width", "byte_width"]
    if not exists(idx):
        print(f"Indexing: {genome}")
        assert which("samtools") is not None, \
            "Samtools is not available in the current PATH"
        run(["samtools", "faidx", genome], check = True)
    fai = pd.read_table(idx, names = faiHeader)
    return fai

def SplitEqualBySize(total, sizeLimit):
    splits = math.ceil(total / sizeLimit)
    splitSize = math.ceil(total / splits)
    result = []
    for i in range(splits):
        if i == 0:
            start = 1
        else:
            start = i * splitSize + 1
        if i + 1 == splits:
            end = total
        else:
            end = (i + 1) * splitSize
        result.append([start, end])
    return(result)

def CreateContigGroups(fai, limit = 2e6):
    # Attempt to correct for wrong inputs
    if not isinstance(fai, pd.DataFrame):
        fai = LoadGenomeIdx(fai)
    if not isinstance(limit, float):
        limit = float(limit)

    # Sort to put the largest values up front
    fai = fai.sort_values("size", ascending = False)

    # Divide contigs over groups that stay below the limit
    groups = []
    groupSize = None
    for i, row in fai.iterrows():
        # If the contig is larger than the limit, split it into regions
        # A limit of 1 disables this behavior
        if row["size"] > limit and limit > 1:
            splits = SplitEqualBySize(row["size"], limit)
            splits = [[f"{row['contig']}\t{x[0]}\t{x[1]}"] for x in splits]
            groups += splits
            continue

        # Combine smaller contigs into a single group
        if not groups or not groupSize:
            groups.append([row["contig"]])
            groupSize = row["size"]
            continue

        # If group size stays below limit add it to the current group
        groupSize += row["size"]
        if groupSize <= limit:
           groups[-1].append(row["contig"])
           continue

        # If it exceeds the limit we create a new group
        groups.append([row["contig"]])
        groupSize = row["size"]

    # Collapse list into comma separated strings
    groups = ["\n".join(x) for x in groups]
    return(groups)

