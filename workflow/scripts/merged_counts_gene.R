################################################################################
###load all the data
################################################################################

dir_files <- "./"

files <- snakemake@input

files <- unique(files)

for(i in 1:length(files)){
    
    if(i == 1) {
        tmp <- read.delim(paste(dir_files,files[i],sep="/"),header=FALSE)
            colnames(tmp)[2] <- gsub("results/htseq_counts/","",gsub("\\.txt$","",files[i]))
        
        output <- tmp   
    } else {
        tmp <- read.delim(paste(dir_files,files[i],sep="/"),header=FALSE)
            colnames(tmp)[2] <- gsub("results/htseq_counts/","",gsub("\\.txt$","",files[i]))
            
        output <- merge(output,tmp,by.x=1,by.y=1)
    }
    cat(i/length(files),"")
}
colnames(output)[1] <- "gene_id"


write.table(output,file="results/merged_counts/count_table_gene.txt",quote=F,sep="\t",col.names = TRUE,row.names=FALSE)
