#!/usr/bin/env python3

import argparse
import os
import subprocess
import sys

# Helper functions
def shell(cmd):
    sys.stderr.write(f"[Wrapper.py] executing: '{cmd}'\n")
    return subprocess.check_call(cmd, shell = True)

# Create CLI
parser = argparse.ArgumentParser()
parser.add_argument("-x", dest="prefix", help="hisat2 database prefix")
parser.add_argument("-S", dest="output", help="name of the alignment (.bam) file")
parser.add_argument("reads", nargs="+",
                    help='one or multiple fastq (delimited by a space)')
parser.add_argument("-12", dest="paired", action="store_true",
                    help="enable paired-end mode")
parser.add_argument("-p", dest="threads", default=1, type=int,
                    help="number of threads to use")
xargs = parser.parse_args()

# Process arguments
basename = os.path.splitext(xargs.output)[0]
reads = xargs.reads
n = len(reads)

# Define fixed part of command, use reheader to remove the @RG cmd form @PG CLI:
hisatCmd = f"hisat2 --dta -p {xargs.threads} -x {xargs.prefix}"
bamCmd = f"samtools view -@ {xargs.threads - 1} -1 -h"
mergeCmd = f"samtools cat"

# Complete command based on input
if xargs.paired:
    sys.stderr.write("[Wrapper.py] running in paired end mode\n")
    assert n % 2 == 0, "Uneven number of paired reads!"

    # If there is more than one set of reads, align them separately and merge
    if n > 2:
        reads.sort()
        alignments = []
        for i in range(0, n, 2):
            tmpFile = f"{basename}_{int(i/2)}.bam"
            shell(f"{hisatCmd} -1 {reads[i]} -2 {reads[i+1]} | {bamCmd} -o {tmpFile}")
            alignments.append(tmpFile)
        shell(f"{mergeCmd} {' '.join(alignments)} -o {xargs.output}")
        shell(f"rm {' '.join(alignments)}")

    else:
        shell(f"{hisatCmd} -1 {reads[0]} -2 {reads[1]} | {bamCmd} -o {xargs.output}")

else:
    sys.stderr.write("[Wrapper.py] running in unpaired mode\n")
    if n > 1:
        alignments = []
        for i in range(n):
            tmpFile = f"{basename}_{i}.bam"
            shell(f"{hisatCmd} -U {reads[i]} | {bamCmd} -o {tmpFile}")
            alignments.append(tmpFile)
        shell(f"{mergeCmd} {' '.join(alignments)} -o {xargs.output}")
        shell(f"rm {' '.join(alignments)}")

    else:
        shell(f"{hisatCmd} -U {reads[0]} | {bamCmd} -o {xargs.output}")
