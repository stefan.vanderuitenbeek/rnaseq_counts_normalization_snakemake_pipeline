This repository is the RNA sequencing counts and normalization pipeline in Snakemake.

https://doi.org/10.12688/f1000research.29032.1


You need to edit a few things in the config/config.yml file:

    1. rename the reference genome (aka .fa, .fasta, .fna)
    2. rename the annotation file (aka .gtf, .gff, .gff3)


The config/runinfo.xlsx (.csv and .tsv formats would also works) needs to be changed to your samples 
-> ID, Paired and Reads columns needs to be set good.


To make this pipeline better then it is already:
- [ ] The rules htseq_count_exon and htseq_count_gene in the workflow/Snakefile needs to be same gene features as the annotation file, 
the parameter -i in the shell can be either gene_id (gtf file) or ID (gff and gff3 file formats).
